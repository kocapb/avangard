<?php


namespace App\Repositories;


use App\Models\Partner;
use Nicoaudy\Repositories\Eloquent\Repository;

/**
 * Class PartnerRepository
 * @package App\Repositories
 */
class PartnerRepository extends Repository
{
    /**
     * @return mixed|string
     */
    public function model()
    {
        return Partner::class;
    }

}
