<?php


namespace App\Repositories;


use App\Models\Product;
use Nicoaudy\Repositories\Eloquent\Repository;

/**
 * Class ProductRepository
 * @package App\Repositories
 */
class ProductRepository extends Repository
{
    public function model()
    {
        return Product::class;
    }

}
