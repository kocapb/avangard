<?php


namespace App\Repositories\Criteria\Order;


use App\Models\Order;
use Nicoaudy\Repositories\Contracts\RepositoryInterface as Repository;
use Nicoaudy\Repositories\Criteria\Criteria;

/**
 * Class OrderStatusDoneCriteria
 * @package App\Repositories\Criteria\Order
 */
class OrderStatusDoneCriteria extends Criteria
{
    public function apply($model, Repository $repository)
    {
        return $model->whereRaw('date(orders.delivery_dt) = date (now())')
            ->where('orders.status', Order::ORDER_STATUS_COMPLETED)
            ->orderBy('orders.delivery_dt', 'desc')
            ->limit(Order::LIMIT);
    }
}
