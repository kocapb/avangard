<?php


namespace App\Services\Order;


use App\Http\Requests\Order\OrderListRequest;
use App\Interfaces\GetServiceInterface;
use App\Models\Order;
use App\Repositories\Criteria\Order\{OrderListCriteria,
    OrderStatusCurrentCriteria,
    OrderStatusDoneCriteria,
    OrderStatusNewCriteria,
    OrderStatusOverdueCriteria};
use App\Repositories\OrderRepository;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ListService
 * @package App\Services\Order
 */
class ListGetService extends OrderCommonService implements GetServiceInterface
{
    /**
     * ListService constructor.
     * @param OrderRepository $repository
     * @param OrderListRequest $request
     */
    public function __construct(OrderRepository $repository, OrderListRequest $request)
    {
        parent::__construct($repository, $request);
    }

    /**
     * @return Collection|Order[]
     */
    public function get()
    {
        $repository = $this->getRepository();
        switch ($this->getRequest()->get('type')) {
            case Order::ORDER_TYPE_NEW:
                $repository = $repository->pushCriteria(new OrderStatusNewCriteria());
                break;
            case Order::ORDER_TYPE_OVERDUE:
                $repository = $repository->pushCriteria(new OrderStatusOverdueCriteria());
                break;
            case Order::ORDER_TYPE_CURRENT:
                $repository = $repository->pushCriteria(new OrderStatusCurrentCriteria());
                break;
            case Order::ORDER_TYPE_DONE:
                $repository = $repository->pushCriteria(new OrderStatusDoneCriteria());
                break;
            case Order::ORDER_TYPE_ALL:
            default:
                break;
        }

        return $repository->with(['partner', 'orderProducts'])
            ->pushCriteria(new OrderListCriteria)
            ->paginate(config('setting.paginate.per_page'));
    }
}
