<?php


namespace App\Services\Weather;

/**
 * Class Model
 * @package App\Services\Weather
 * @property string $temp
 */
class Model
{
    /** @var string */
    protected string $temp;

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->$name;
        }
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        }

        return $this;
    }
}
