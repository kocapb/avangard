<?php


namespace App\Services\Weather\Yandex;


use App\Exceptions\ClientWeatherException;
use App\Exceptions\WeatherException;
use App\Services\Weather\ClientAdapter;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Response;

/**
 * Class Client
 * @package App\Services\Weather\Yandex
 */
class Client extends ClientAdapter
{
    /**
     * @throws WeatherException
     * @throws ClientWeatherException|GuzzleException
     */
    protected function getApiResult()
    {
        if (!$url = $this->getUrl()) {
            throw new WeatherException(trans('weather.api.url_not_set'));
        }

        if (!$apiKey = $this->getApiKey()) {
            throw new WeatherException(trans('weather.api.key_not_set'));
        }

        if (!$params = $this->getApiParams()) {
            throw new WeatherException(trans('weather.api.params_not_set'));
        }

        $response = (new GuzzleHttpClient())->get($url, [
            'headers' => [
                'X-Yandex-API-Key' => $apiKey,
                'Content-Type' => 'application/json',
            ],
            'query' => $params,
        ]);

        if ($response->getStatusCode() === Response::HTTP_OK) {
            return json_decode($response->getBody()->getContents(), true);
        }

        throw new ClientWeatherException(trans('weather.client.fail_response'), $response->getStatusCode());

    }
}
