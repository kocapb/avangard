<?php


namespace App\Repositories\Criteria\Order;


use App\Models\Order;
use Nicoaudy\Repositories\Contracts\RepositoryInterface as Repository;
use Nicoaudy\Repositories\Criteria\Criteria;

/**
 * Class OrderStatusNewCriteria
 * @package App\Repositories\Criteria\Order
 */
class OrderStatusNewCriteria extends Criteria
{
    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        return $model->whereRaw('orders.delivery_dt > now()')
            ->where('orders.status', Order::ORDER_STATUS_NEW)
            ->orderBy('orders.delivery_dt', 'asc')
            ->limit(Order::LIMIT);
    }

}
