<?php


namespace App\Services\Weather;


use App\Interfaces\ClientAdapterInterface;

/**
 * Class Client
 * @package App\Services\Weather
 */
abstract class ClientAdapter implements ClientAdapterInterface
{
    /**
     * Обращение к сервису погоды
     *
     * @return mixed
     */
    public function result()
    {
        return $this->getApiResult();
    }

    abstract protected function getApiResult();

    /**
     * @return string
     */
    protected function getUrl(): string
    {
        return config('weather.url');
    }

    /**
     * @return string
     */
    protected function getApiKey(): string
    {
        return config('weather.api_key');
    }

    /**
     * @return array
     */
    protected function getApiParams(): array
    {
        return config('weather.params');
    }
}
