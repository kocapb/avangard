<?php


namespace App\Http\Requests\Order;


use App\Http\Requests\Request;
use App\Models\Order;
use Illuminate\Validation\Rule;

/**
 * Class OrderListRequest
 * @package App\Http\Requests\Order
 */
class OrderListRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'type' => Rule::in(Order::getAvailableTypes())
        ];
    }
}
