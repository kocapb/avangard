<?php
use App\Models\Order;
return [
    'statuses' => [
        'new' => 'new',
        'confirmed' => 'confirmed',
        'completed' => 'completed',
    ],
    'types' => [
        Order::ORDER_TYPE_OVERDUE => 'Overdue',
        Order::ORDER_TYPE_CURRENT => 'Current',
        Order::ORDER_TYPE_NEW => 'New',
        Order::ORDER_TYPE_DONE => 'Done',
        Order::ORDER_TYPE_ALL => 'All',
    ],
    'email' => 'Client email',
    'partner' => 'partner',
    'cost' => 'cost',
    'status' => [
        'title' => 'status',
        'select' => 'Choose a status of order'
    ],
    'composition' => 'composition',
    'quantity' => 'quantity'
];
