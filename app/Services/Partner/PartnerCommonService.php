<?php


namespace App\Services\Partner;


use App\Repositories\PartnerRepository;
use App\Services\CommonService;

/**
 * Class PartnerCommonService
 * @package App\Services\Partner
 */
class PartnerCommonService extends CommonService
{
    /**
     * PartnerCommonService constructor.
     * @param PartnerRepository $repository
     */
    public function __construct(PartnerRepository $repository)
    {
        parent::__construct($repository);
    }
}
