<?php


namespace App\Repositories\Criteria\Order;


use App\Models\Order;
use Nicoaudy\Repositories\Contracts\RepositoryInterface as Repository;
use Nicoaudy\Repositories\Criteria\Criteria;

/**
 * Class OrderStatusOverdueCriteria
 * @package App\Repositories\Criteria\Order
 */
class OrderStatusOverdueCriteria extends Criteria
{
    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        return $model->whereRaw('orders.delivery_dt < now()')
            ->where('orders.status', Order::ORDER_STATUS_CONFIRMED)
            ->orderBy('orders.delivery_dt', 'desc')
            ->limit(Order::LIMIT);
    }
}
