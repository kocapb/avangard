<?php


namespace App\Services\Partner;


use App\Interfaces\GetServiceInterface;

/**
 * Class ListGetService
 * @package App\Services\Partner
 */
class ListGetService extends PartnerCommonService implements GetServiceInterface
{
    /**
     * @return mixed
     */
    public function get()
    {
        return $this->getRepository()->all();
    }
}
