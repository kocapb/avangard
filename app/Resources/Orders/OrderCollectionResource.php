<?php


namespace App\Resources\Orders;


use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class OrderCollectionResource
 * @package App\Resources\Orders
 */
class OrderCollectionResource extends ResourceCollection
{

}
