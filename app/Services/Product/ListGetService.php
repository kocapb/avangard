<?php


namespace App\Services\Product;


use App\Interfaces\GetServiceInterface;

/**
 * Class ListService
 * @package App\Services\Product
 */
class ListGetService extends ProductCommonService implements GetServiceInterface
{
    /**
     * @return mixed
     */
    public function get()
    {
        return $this->getRepository()->all();
    }
}
