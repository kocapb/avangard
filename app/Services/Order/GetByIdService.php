<?php


namespace App\Services\Order;


use App\Interfaces\GetByIdServiceInterface;
use App\Repositories\Criteria\Order\OrderEditCriteria;

/**
 * Class GetService
 * @package App\Services\Order
 */
class GetByIdService extends OrderCommonService implements GetByIdServiceInterface
{
    /**
     * @param int $id
     * @return mixed
     */
    public function get(int $id)
    {
        return $this->getRepository()
            ->pushCriteria(new OrderEditCriteria($id))
            ->with(['partner', 'orderProducts', 'products'])
            ->all()
            ->first();
    }

}
