<?php
return [
    'url' => 'https://api.weather.yandex.ru/v2/forecast',
    'api_key' => '3c7cd522-0639-464d-bb3a-3c64c36f6d4f',
    'params' => [
        'lat' => '53.2520900',
        'lon' => '34.3716700',
        'lang' => 'ru_RU'
    ],
];
