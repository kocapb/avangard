<?php


namespace App\Services\Order;


use App\Http\Requests\Order\OrderEditRequest;
use App\Repositories\OrderRepository;
use App\Services\CommonEditService;

/**
 * Class EditService
 * @package App\Services\Order
 */
class EditService extends CommonEditService
{
    /**
     * EditService constructor.
     * @param OrderRepository $repository
     * @param OrderEditRequest $request
     */
    public function __construct(OrderRepository $repository, OrderEditRequest $request)
    {
        parent::__construct($repository, $request);
    }
}
