<?php


namespace App\Services\Order;


use App\Http\Requests\Request;
use App\Repositories\OrderRepository;
use App\Services\CommonService;

/**
 * Class OrderCommonService
 * @package App\Services\Order
 */
class OrderCommonService extends CommonService
{
    /**
     * OrderCommonService constructor.
     * @param OrderRepository $repository
     * @param Request $request
     */
    public function __construct(OrderRepository $repository, Request $request)
    {
        parent::__construct($repository, $request);
    }

}
