<?php


namespace App\Interfaces;

/**
 * Interface ServiceInterface
 * @package App\Services
 */
interface GetServiceInterface
{
    public function get();
}
