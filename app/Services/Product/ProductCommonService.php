<?php


namespace App\Services\Product;


use App\Repositories\ProductRepository;
use App\Services\CommonService;

/**
 * Class ProductCommonService
 * @package App\Services\Product
 */
abstract class ProductCommonService extends CommonService
{
    /**
     * ProductCommonService constructor.
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        parent::__construct($repository);
    }
}
