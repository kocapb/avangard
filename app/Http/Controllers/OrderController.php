<?php


namespace App\Http\Controllers;


use App\Models\Order;
use App\Services\Order\{EditService, GetByIdService, ListGetService};
use App\Services\Partner\ListGetService as PartnerListGetService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class OrderController extends Controller
{
    /**
     * @param ListGetService $service
     * @return Factory|Application|View
     */
    public function list(ListGetService $service)
    {
        return view('order.index', [
            'records' => $service->get(),
        ]);
    }

    /**
     * @param $id
     * @param GetByIdService $service
     * @param PartnerListGetService $partnerListService
     * @return Factory|Application|View
     */
    public function getEdit($id,
                            GetByIdService $service,
                            PartnerListGetService $partnerListService)
    {
        /** @var Order $record */
        $record = $service->get($id);
        return view('order.edit', [
            'record' => $record,
            'partners' => $partnerListService->get()->pluck('name', 'id'),
            'orderProducts' => $record->getOrderProducts()->keyBy('product_id'),
            'statuses' => Order::getStatuses(),
        ]);
    }

    /**
     * @param int $id
     * @param EditService $service
     * @return Application|RedirectResponse|Redirector
     */
    public function postEdit(int $id, EditService $service)
    {
        $service->get($id);
        return redirect()->route('order.get.list');
    }
}
