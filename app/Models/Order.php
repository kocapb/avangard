<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\{HasMany, HasOne};
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Order
 * @package App\Models
 */
class Order extends Model
{
    /** @var int */
    const ORDER_STATUS_NEW = 0;
    /** @var int */
    const ORDER_STATUS_CONFIRMED = 10;
    /** @var int */
    const ORDER_STATUS_COMPLETED = 20;

    /** @var string */
    const ORDER_TYPE_OVERDUE = 'overdue';
    /** @var string */
    const ORDER_TYPE_CURRENT = 'current';
    /** @var string */
    const ORDER_TYPE_NEW = 'new';
    /** @var string */
    const ORDER_TYPE_DONE = 'done';
    /** @var string */
    const ORDER_TYPE_ALL = 'all';

    /** @var int */
    const LIMIT = 50;

    /**
     * @return string[]
     */
    public static function getAvailableTypes()
    {
        return [
            self::ORDER_TYPE_OVERDUE,
            self::ORDER_TYPE_CURRENT,
            self::ORDER_TYPE_NEW,
            self::ORDER_TYPE_DONE,
            self::ORDER_TYPE_ALL,
        ];
    }

    /**
     * Get Available statuses
     *
     * @return int[]
     */
    public static function getAvailableStatuses()
    {
        return [
            self::ORDER_STATUS_NEW,
            self::ORDER_STATUS_CONFIRMED,
            self::ORDER_STATUS_COMPLETED,
        ];
    }

    /**
     * @param int $code
     * @return string
     */
    public static function getStatusNameByCode(int $code)
    {
        if (isset(self::getStatuses()[$code])) {
            return self::getStatuses()[$code];
        }

        return '';
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::ORDER_STATUS_NEW => trans('order.statuses.new'),
            self::ORDER_STATUS_CONFIRMED => trans('order.statuses.confirmed'),
            self::ORDER_STATUS_COMPLETED => trans('order.statuses.completed'),
        ];
    }

    /**
     * @return Partner|HasOne|null
     */
    public function getPartner()
    {
        return $this->partner()->first();
    }

    /**
     * @return HasOne
     */
    public function partner()
    {
        return $this->hasOne(Partner::class, 'id', 'partner_id');
    }

    /**
     * @return Collection|OrderProduct[]
     */
    public function getOrderProducts()
    {
        return $this->orderProducts()->get()->keyBy('product_id');
    }

    /**
     * @return HasMany
     */
    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class, 'order_id', 'id');
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts()
    {
        return $this->products()->get();
    }

    /**
     * @return BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_products');
    }

}
