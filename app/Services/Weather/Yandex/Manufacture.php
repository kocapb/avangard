<?php


namespace App\Services\Weather\Yandex;

use App\Exceptions\WeatherException;
use App\Interfaces\WeatherServiceInterface;
use App\Services\Weather\ClientAdapter;
use App\Services\Weather\Model;

/**
 * Class Manufacture
 * @package App\Services\Weather
 */
class Manufacture implements WeatherServiceInterface
{
    /** @var ClientAdapter */
    protected ClientAdapter $client;

    /**
     * Manufacture constructor.
     * @param ClientAdapter $client
     */
    public function __construct(ClientAdapter $client)
    {
        $this->client = $client;
    }

    /**
     * Класс для обработки ответа от сервиса погоды
     *
     * @return Model
     * @throws WeatherException
     */
    public function result(): Model
    {
        $result = $this->getWeatherData();
        if (empty($result['fact']['temp'])) {
            throw new WeatherException('exception.empty_object', ['object' => 'forecasts']);
        }

        $model = new Model();
        $model->temp = $result['fact']['temp'];
        return $model;
    }

    /**
     * @return mixed
     */
    protected function getWeatherData()
    {
        return $this->client->result();
    }
}
