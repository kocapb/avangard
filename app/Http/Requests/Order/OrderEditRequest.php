<?php


namespace App\Http\Requests\Order;

use App\Http\Requests\Request;
use App\Models\Order;
use Illuminate\Validation\Rule;

/**
 * Class OrderEditRequest
 * @package App\Requests\Order
 */
class OrderEditRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'client_email' => 'required|email',
            'partner_id' => 'required|int',
            'status' => Rule::in(Order::getAvailableStatuses()),
        ];
    }
}
