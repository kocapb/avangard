<?php

namespace App\Interfaces;

/**
 * Interface ClientAdapterInterface
 * @package App\Services\Weather
 */
interface ClientAdapterInterface
{
    public function result();
}
