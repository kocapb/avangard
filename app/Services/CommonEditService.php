<?php


namespace App\Services;


use App\Interfaces\GetByIdServiceInterface;

/**
 * Class CommonEditService
 * @package App\Services
 */
abstract class CommonEditService extends CommonService implements GetByIdServiceInterface
{

    /**
     * @param int $id
     * @return mixed
     */
    public function get(int $id)
    {
        return $this->getRepository()->update($this->getRequest()->all(), $id);
    }
}
