<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
 * Weather group
 */
Route::group([
    'prefix' => 'weather',
    'as' => 'weather.'
], function () {
    Route::get('index', 'WeatherController@index');
});

/**
 * Order group
 */
Route::group([
    'prefix' => 'order',
    'as' => 'order.'
], function () {
    Route::get('list', 'OrderController@list')->name('get.list');
    Route::get('edit/{id}', 'OrderController@getEdit')->name('get.edit');
    Route::post('edit/{id}', 'OrderController@postEdit')->name('post.edit');
});
