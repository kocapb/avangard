<?php


namespace App\Repositories\Criteria\Order;


use Illuminate\Support\Facades\DB;
use Nicoaudy\Repositories\Contracts\RepositoryInterface as Repository;
use Nicoaudy\Repositories\Criteria\Criteria;

/**
 * Class OrderListCriteria
 * @package App\Repositories\Criteria\Order
 */
class OrderListCriteria extends Criteria
{
    public function apply($model, Repository $repository)
    {
        return $model->select('orders.*',
            DB::raw('sum(order_products.quantity * order_products.price) as cost'),
            DB::raw('array_to_string(array_agg(products.name),\', \') as composite'))
            ->join('order_products', 'orders.id', '=', 'order_products.order_id')
            ->join('products', 'products.id', '=', 'order_products.product_id')
            ->groupBy('order_products.order_id', 'orders.id')
            ->orderBy('orders.id');
    }

}
