<?php

namespace App\Providers;

use App\Interfaces\WeatherServiceInterface;
use App\Services\Weather\ClientAdapter;
use App\Services\Weather\Yandex\Client;
use App\Services\Weather\Yandex\Manufacture;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(WeatherServiceInterface::class, Manufacture::class);
        $this->app->bind(ClientAdapter::class, Client::class);
    }
}
