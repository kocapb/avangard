<?php


namespace App\Repositories;


use App\Models\Order;
use Nicoaudy\Repositories\Eloquent\Repository;

/**
 * Class OrderRepository
 * @package App\Repositories
 */
class OrderRepository extends Repository
{
    /**
     * @return mixed|string
     */
    public function model()
    {
        return Order::class;
    }

}
