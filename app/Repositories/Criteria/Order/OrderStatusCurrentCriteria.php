<?php


namespace App\Repositories\Criteria\Order;


use App\Models\Order;
use Nicoaudy\Repositories\Contracts\RepositoryInterface as Repository;
use Nicoaudy\Repositories\Criteria\Criteria;

class OrderStatusCurrentCriteria extends Criteria
{
    public function apply($model, Repository $repository)
    {
        return $model->whereRaw('orders.delivery_dt BETWEEN NOW() AND NOW() + INTERVAL \'24 HOURS\'')
            ->where('orders.status', Order::ORDER_STATUS_CONFIRMED)
            ->orderBy('orders.delivery_dt', 'asc');
    }
}
