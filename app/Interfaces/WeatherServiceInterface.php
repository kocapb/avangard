<?php


namespace App\Interfaces;

use App\Services\Weather\Model;

/**
 * Interface WeatherServiceInterface
 * @package App\Services\Weather
 */
interface WeatherServiceInterface
{
    public function result(): Model;
}
