<?php
return [
    'empty_object' => 'Empty object response :object',
    'weather' => [
        'api' => [
            'url_not_set' => 'Weather api url not define',
            'key_not_set' => 'Weather api key not define',
            'params_not_set' => 'Weather api params not define',
        ],
        'client' => [
            'fail_response' => 'Fail response',
        ],
    ],
];
