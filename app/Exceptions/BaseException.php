<?php


namespace App\Exceptions;


use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BaseException
 * @package App\Exceptions
 */
class BaseException extends Exception
{
    /** @var string */
    const STATUS_FAIL = 'fail';

    /** @var int */
    protected $code = 500;

    /**
     * @param Request $request
     * @return ResponseFactory|Application|Response
     */
    public function render(Request $request)
    {
        return response([
            'status' => self::STATUS_FAIL,
            'message' => $this->getMessage()], $this->code);
    }
}
