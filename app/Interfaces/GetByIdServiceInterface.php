<?php


namespace App\Interfaces;

/**
 * Interface GetServiceInterface
 * @package App\Interfaces
 */
interface GetByIdServiceInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function get(int $id);
}
