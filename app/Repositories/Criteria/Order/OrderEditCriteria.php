<?php


namespace App\Repositories\Criteria\Order;


use Illuminate\Support\Facades\DB;
use Nicoaudy\Repositories\Contracts\RepositoryInterface as Repository;
use Nicoaudy\Repositories\Criteria\Criteria;

/**
 * Class OrderEditCriteria
 * @package App\Repositories\Criteria\Order
 */
class OrderEditCriteria extends Criteria
{
    /** @var int */
    private int $id;

    /**
     * OrderEditCriteria constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function apply($model, Repository $repository)
    {
        return $model->select('orders.*',
            DB::raw('sum(order_products.quantity * order_products.price) as cost'))
            ->join('order_products', 'orders.id', '=', 'order_products.order_id')
            ->where('orders.id', $this->id)
            ->groupBy('order_products.order_id', 'orders.id');
    }

}
