<div class="tabs">
    @foreach(\App\Models\Order::getAvailableTypes() as $type)
        @include('elements.order-type-link', ['type' => $type])
    @endforeach
</div>
@if($records->count())
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">{{ trans('order.partner') }}</th>
                <th scope="col">{{ trans('order.cost') }}</th>
                <th scope="col">{{ trans('order.composition') }}</th>
                <th scope="col">{{ trans('order.status.title') }}</th>
                <th scope="col">{{ trans('order.edit') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($records as $record)
                <tr>
                    <th scope="row">{{ $record->id }}</th>
                    <td>{{ $record->getPartner()->name }}</td>
                    <td>{{ $record->cost }}</td>
                    <td>{{ $record->composite }}</td>
                    <td>{{ \App\Models\Order::getStatusNameByCode($record->status)}}</td>
                    <td> <a href="{{ route('order.get.edit', ['id' => $record->id]) }}">{{ trans('order.edit') }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div>
        {{ $records->links() }}
    </div>
@endif
