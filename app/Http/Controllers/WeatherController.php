<?php


namespace App\Http\Controllers;

use App\Interfaces\WeatherServiceInterface;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\View\View;

/**
 * Class WeatherController
 * @package App\Http\Controllers
 */
class WeatherController extends Controller
{
    /**
     * @param WeatherServiceInterface $weatherService
     * @return Factory|Application|View
     */
    public function index(WeatherServiceInterface $weatherService)
    {
        return view('weather.index', [
            'weather' => $weatherService->result()
        ]);
    }
}
