<form action="{{ route('order.post.edit', ['id' => $record->id]) }}" method="post">
    <div class="form-group row">
        <label for="inputEmail" class="control-label">{{ trans('order.email') }}</label>
        <input id="inputEmail"
               name="client_email"
               type="email"
               class="form-control border-radius"
               placeholder="Email"
               value="@if(isset($record)){{ $record->client_email }}@else{{ Input::old('email') }}@endif"
               data-error="{{ trans('error.email') }}"
               required
        >
        <div class="help-block with-errors"></div>
    </div>

    <div class="form-group">
        <label for="selectPartner" class="control-label">{{ trans('order.partner') }}</label>
        {{
           Form::select('partner_id', $partners, isset($record) ? $record->partner_id : null, [
               'placeholder' => trans('partner.select'),
               'id' => 'partnerId',
           ])
        }}
    </div>

    <div class="form-group row">
        <label for="products">{{ trans('product.title') }} - {{ trans('order.quantity') }}</label>
        <div class="product-box">
            @foreach($record->getProducts() as $product)
                <div>{{ $product->name }} - {{ $orderProducts[$product->id]->quantity }}</div>
            @endforeach
        </div>
    </div>

    <div class="form-group row">
        <label for="status">{{ trans('order.status.title') }} </label>
        {{
            Form::select('status', $statuses, isset($record) ? $record->status : null, [
               'placeholder' => trans('order.status.select'),
               'id' => 'status',
           ])
        }}
    </div>

    <div class="form-group row">
        <label for="cost">{{ trans('order.cost') }}</label>
        <input readonly="readonly" value="{{ $record->cost }}"></input>
    </div>
    {!! csrf_field() !!}
    <button type="submit">{{ trans('app.save') }}</button>

</form>
